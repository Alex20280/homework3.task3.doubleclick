package layouts.sourceit.com.homework3task3doubleclick;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textViewMain;
    Button button;
    int i =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewMain = (TextView) findViewById(R.id.textViewMain);
        button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i++;

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (i ==1) {
                            Intent intent = new Intent(MainActivity.this, Activity2.class);
                            startActivity(intent);
                        }else if (i == 2){
                            Intent intent2 = new Intent(MainActivity.this, Activity3.class);
                            startActivity(intent2);
                        }
                        i = 0;
                    }
                }, 500);
            }
        });

    }


}
